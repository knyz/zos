# ZOS OPERATING SYSTEM

"I hate computers so much, 
I wrote an operating system,
I still hate computers"

~ Vilyaem

ZOS (zaws) is a free single-threaded single-user operating system written from scratch 
in C (ANSI C89), Assembly, and S (its own programming language). ZOS is for the x86-64
and *duino architectures. ZOS features a boot loader, kernel, interpreter, shell, text editor,
graphics, core utilities, and (optional) filesystem. ZOS' objective is to be an ultrafunctionalist high performant 'suckless'
operating system with a heavy focus on simplicity and freedom, ZOS is also designed to be the primary operating
system for the quickly approaching collapse of modern secular society. 
ZOS by default, does not come with a graphical interface, desktop environment, or window manager, 
it is primarily command line driven. ZOS has no internet/network connectivity, GPU support, segmentation,
rings, rootkit, proprietary blobs, WiFi card support, multiple disk support, support for other filesystems (FAT,ext,NTFS...),
or USB support, all of these are positives.
ZOS only supports its own file system, MiniFS. Operating systems that are most similar, either conceptually
or ideologically are 86-DOS (1980), TempleOS (2018), and CollapseOS. ZOS is NOT based off of Unix or DOS.
ZOS uses an optional RAM based filesystem, and only very rarily writes to disk, this contributes greatly to the performance
of the system, as a hard disk is the slowest form of memory to access and alter, also some systems, especially embedded
devices, do not have harddisks.

ZOS can easily be compiled and emulated on POSIX, DOS, and Windows machines 
so that users may develop or learn the operating system.

---

I, Vilyaem, have created ZOS over the course of a couple weeks.
I wanted a system that could be for once considered both 'lightweight' and effective.
The system with its clear and sober mission absolutely clobbers
horrid disasters such as gnooh/Linux and Windows, software should be designed
absolutely perfectly and last for eternity, akin to the Roman Empire,
or the word of God. This is not even the goal of most software production,
so its not a mystery why modern computing is an unending disaster.

---

Line count/complexity comparison:

Windows - >50,000,000 proprietary lines


Linux - >36,000,000 lines


Temple OS - 131,000 lines


FreeDOS - 32,000 lines


Dusk OS - 10,000 lines


86DOS - 7100 lines


ZOS - <2,500 lines



---

Screenshots:

![A user writing a program with the Vedlin text editor](screenshot1.PNG "A user writing a program with the Vedlin text editor")

![A 3D graphical video game written in S running on ZOS](screenshot2.PNG "A 3D graphical video game written in S running on ZOS")

---

## CONTRIBUTION, WORKFLOW, SOURCE GUIDE

There is no 'c*de of c*nduct'.
The issue/bug tracker is in the 'bug' file. Keeps this project independent from online issue tracking hosts.

Filemap:
.


├── README.md -- this file


├── bug -- bugs & issues go here


├── c.sh -- compiling script


├── main.c -- the entire source code


└── zos -- emulator executable



1 directory, 5 files

run ``./c.sh`` to compile the simulator.


run ``./c.sh X86`` to compile a bootable x86-64 image.


run ``./c.sh DUINO`` to compile for arduino.



Importance scale in issue tracking
1 - last concern
2 - important
3 - right now!

## STATUS

ZOS is under heavy construction, at this point it is a proof of concept.

## PHILANTHROPY

_official ZOS donation fund_

Monero (XMR): 48Sxa8J6518gqp4WeGtQ4rLe6SctPrEnnCqm6v6ydjLwRPi9Uh9gvVuUsU2AEDw75meTHCNY8KfU6Txysom4Bn5qPKMJ75w


Wownero (WOW): WW2L2yC6DMg7GArAH3nqXPA6UBoRogf64GodceqA32SeZQpx27xd6rqN82e36KE48a8SAMSoXDB5WawAgVEFKfkw1Q5KSGfX9

If you have philanthropic interest in ZOS, contact Vilyaem.


---

## LICENSE

`
CHRISTIAN FREE SOFTWARE LICENSE
CFSL

This software is free and open source charity ware, users are asked
to donate to the Eastern Orthodox Church by any means.

Redistribution of this project in source and/or binary forms with/without
modification, are permitted provided that the following conditions are met:

1. Redistributions must retain this notice, the conditions, and the disclaimer.
2. Redistributions must retain credit to the author, and signage to where the original
work can be found.
3. Redistributions cannot become a part of, in anyway shape or form, part of proprietary
software, or software that is clearly out of line with Christian values.
4. Redistributions must remain free, both in price, and what users may do with the software,
the software must remain public and easily accessible.

DISCLAIMER

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
`
